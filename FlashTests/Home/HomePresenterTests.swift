//
//  HomePresenterTests.swift
//  FlashTests
//
//  Created by Karan Kumar on 30/05/19.
//  Copyright © 2019 Karan Kumar. All rights reserved.
//

import XCTest
import Alamofire
@testable import Flash

class HomePresenterTests: XCTestCase {
    var sut: HomePresenter!
    var mockView: MockView!
    var mockRouter: MockRouter!
    var mockInteractor: HomeInteractor!
    
    override func setUp() {
        super.setUp()
        mockView = MockView()
        mockRouter = MockRouter()
        mockInteractor = HomeInteractor()
        sut = HomePresenter(router: mockRouter, view: mockView, interactor: mockInteractor)
    }
    
    override func tearDown() {
        super.tearDown()
        mockView = nil
        mockRouter = nil
        mockInteractor = nil
        sut = nil
    }
    
    func testGetVehiclesRequest() {
        let expectations = expectation(description: "Vehicles Request complete")
        mockView.asyncExpectation = expectations
        
        sut.fetchVehicles()
        
        waitForExpectations(timeout: 15) { [weak self] error in
            if let error = error {
                XCTFail("Timeout error: \(error)")
            }
            
            guard let result = self?.mockView.asyncResult else {
                XCTFail("Error geting vehicles from list end point")
                return
            }
            
            XCTAssertTrue(result)
        }
    }
    
    func testGetVehicleDetailRequest() {
        let expectations = expectation(description: "Vehicle Detail Request complete")
        mockView.asyncExpectation = expectations
        
        let vehicleId = 1
        sut.fetchVehicleDetail(vehicleId: vehicleId)
        
        waitForExpectations(timeout: 15) { [weak self] error in
            if let error = error {
                XCTFail("Timeout error: \(error)")
            }
            
            guard let result = self?.mockView.asyncResult else {
                XCTFail("Error geting vehicle detail for id \(String(vehicleId))")
                return
            }
            
            XCTAssertTrue(result)
        }
    }
}

class MockView: HomeViewInterface, VehicleDetailResponseInterface {
    var asyncResult: Bool?
    var asyncExpectation: XCTestExpectation?
    
    func reloadVehicles() {
        asyncResult = true
        asyncExpectation?.fulfill() // got response
    }
    
    func didGetVehicleDetail(vehicle: Vehicle) {
        if vehicle.id == 1 {
            asyncResult = true
        } else {
            asyncResult = false
        }
        asyncExpectation?.fulfill() // got response
    }
    
    func showAlert(with title: String?, message: String?, actions: [UIAlertAction]) {
        if let error = title?.contains("Unable to fetch vehicles") {
            asyncResult = error
            asyncExpectation?.fulfill() // got response
        } else if let error = title?.contains("Unable to fetch vehicle detail") {
            asyncResult = error
            asyncExpectation?.fulfill() // got response
        }
    }
    func failToGetVehicleDetail(vehicleId: Int) {}
    func setLoadingVisible(_ visible: Bool) {}
}

class MockRouter: HomeRouterInterface {
    static func createModule()-> UIViewController {return UIViewController()}
}
