//
//  VehicleTests.swift
//  FlashTests
//
//  Created by Karan Kumar on 29/05/19.
//  Copyright © 2019 Karan Kumar. All rights reserved.
//

import XCTest
@testable import Flash

class VehicleTests: XCTestCase {
    override func setUp() {
        super.setUp()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testInitWithJson() {
        let json = "{\"id\":1,\"name\":\"000011\",\"description\":\"Electric Scooter\",\"latitude\":52.529077,\"longitude\":13.416351,\"batteryLevel\":98,\"timestamp\":\"2019-03-10T09:31:56Z\",\"price\":15,\"priceTime\":60,\"currency\":\"€\"}"
        
        guard let vehicle = Vehicle(JSONString: json) else {
            XCTFail("Error initialising vehicle from json")
            return
        }
        XCTAssertEqual(vehicle.id, 1)
        XCTAssertEqual(vehicle.name, "000011")
        XCTAssertEqual(vehicle.description, "Electric Scooter")
        XCTAssertEqual(vehicle.latitude, 52.529077)
        XCTAssertEqual(vehicle.longitude, 13.416351)
        XCTAssertEqual(vehicle.batteryLevel, 98)
        XCTAssertEqual(vehicle.timestamp, "2019-03-10T09:31:56Z")
        XCTAssertEqual(vehicle.price, 15)
        XCTAssertEqual(vehicle.currency, "€")
        XCTAssertEqual(vehicle.priceTime, 60)
    }
    func testInitWithPartialJson() {
        let json = "{\"id\":1,\"name\":\"000011\",\"description\":\"Electric Scooter\"}"
        
        guard let vehicle = Vehicle(JSONString: json) else {
            XCTFail("Error initialising vehicle from json")
            return
        }
        XCTAssertEqual(vehicle.id, 1)
        XCTAssertEqual(vehicle.name, "000011")
        XCTAssertEqual(vehicle.description, "Electric Scooter")
    }
}
