//
//  VehicleServicesTests.swift
//  FlashTests
//
//  Created by Karan Kumar on 30/05/19.
//  Copyright © 2019 Karan Kumar. All rights reserved.
//

import XCTest
import Alamofire
@testable import Flash

class VehicleServicesTests: XCTestCase {
    
    var sut: VehiclesService!
    
    override func setUp() {
        super.setUp()
        sut = VehiclesService()
    }
    
    override func tearDown() {
        super.tearDown()
        sut = nil
    }
    
    func testVehicleListEndpoint() {
        sut.getVehicles { (response: DataResponse<[Vehicle]>) in
            if response.result.value != nil {
                XCTAssertTrue(true)
            } else if let error = response.result.error {
                XCTFail("Vehicle list api request fail: \(error.localizedDescription)")
            } else {
                XCTFail("Vehicle list api request fail: unknownError")
            }
        }
    }
    
    func testVehicleDetailEndpoint() {
        let mockId = 2
        sut.getVehicleDetail(mockId) { (response: DataResponse<Vehicle>) in
            if let vehicle = response.result.value {
                if vehicle.id == mockId {
                    XCTAssertTrue(true)
                } else {
                    XCTFail("Vehicle detail fetched is wrong. Fetched for id:\(mockId) and got \(String(describing: vehicle.id))")
                }
            } else if let error = response.result.error {
                XCTFail("Vehicle detail api request fail: \(error.localizedDescription)")
            } else {
                XCTFail("Vehicle detail api request fail: unknownError")
            }
        }
    }
}
