//
//  VehiclesService.swift
//  Flash
//
//  Created by Karan Kumar on 28/05/19.
//  Copyright © 2019 Karan Kumar. All rights reserved.
//

import UIKit
import AlamofireObjectMapper
import Alamofire

typealias VehiclesListCompletionBlock = (DataResponse<[Vehicle]>) -> (Void)
typealias VehicleDetailCompletionBlock = (DataResponse<Vehicle>) -> (Void)

class VehiclesService {
    
    @discardableResult
    func getVehicles(_ completion: @escaping VehiclesListCompletionBlock) -> DataRequest {
        
        let url = Constants.API.BASE_URL + Constants.API.GET_VEHICLES_END_POINT
        return Alamofire.request(url).responseArray { (response: DataResponse<[Vehicle]>) in
            completion(response)
        }
    }
    
    @discardableResult
    func getVehicleDetail(_ vehicleId: Int, _ completion: @escaping VehicleDetailCompletionBlock) -> DataRequest {
        
        let url = Constants.API.BASE_URL + Constants.API.VEHICLES_DETAIL_END_POINT + String(vehicleId)
        return Alamofire.request(url).responseObject { (response: DataResponse<Vehicle>) in
            completion(response)
            NSLog("Fetched for \(vehicleId)")
        }
    }
    
}
