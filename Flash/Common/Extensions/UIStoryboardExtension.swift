//
//  UIStoryboardExtension.swift
//  Flash
//
//  Created by Karan Kumar on 28/05/19.
//  Copyright © 2019 Karan Kumar. All rights reserved.
//

import UIKit

extension UIStoryboard {
    
    func instantiateViewController<T: UIViewController>(ofType _: T.Type, withIdentifier identifier: String? = nil) -> T {
        let identifier = identifier ?? String(describing: T.self) // default identifier as the name itself
        return instantiateViewController(withIdentifier: identifier) as! T
    }
    
}
