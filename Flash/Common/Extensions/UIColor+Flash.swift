//
//  UIColor+Flash.swift
//  Flash
//
//  Created by Karan Kumar on 27/05/19.
//  Copyright © 2019 Karan Kumar. All rights reserved.
//

import UIKit

extension UIColor {
    
    static let flashThemeColor: UIColor = UIColor(
        red: 255.0/255.0,
        green: 109.0/255.0,
        blue: 141.0/255.0,
        alpha: 1.0
    )
    
}
