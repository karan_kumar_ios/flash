//
//  ViewInterface.swift
//  Flash
//
//  Created by Karan Kumar on 28/05/19.
//  Copyright © 2019 Karan Kumar. All rights reserved.
//

import UIKit
import SVProgressHUD

protocol ViewInterface: class {
    func showProgressHUD()
    func hideProgressHUD()
    func showAlert(with title: String?, message: String?, actions: [UIAlertAction])
}

extension ViewInterface {
    
    func showProgressHUD() {
        SVProgressHUD.show()
    }
    
    func hideProgressHUD() {
        SVProgressHUD.dismiss()
    }
    
    func showErrorAlert(with message: String?) {
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        showAlert(with: "Something went wrong", message: message, actions: [okAction])
    }
    
    func showAlert(with title: String?, message: String?) {
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        showAlert(with: title, message: message, actions: [okAction])
    }
}



