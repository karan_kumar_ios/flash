//
//  RouterProtocols.swift
//  Flash
//
//  Created by Karan Kumar on 27/05/19.
//  Copyright © 2019 Karan Kumar. All rights reserved.
//

import Foundation
import UIKit

protocol RouterInterface: class {
    static func createModule()-> UIViewController
}
