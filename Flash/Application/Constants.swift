//
//  Constants.swift
//  Flash
//
//  Created by Karan Kumar on 27/05/19.
//  Copyright © 2019 Karan Kumar. All rights reserved.
//

struct Constants {
    struct API {
        static let BASE_URL = "https://my-json-server.typicode.com"
        static let GET_VEHICLES_END_POINT = "/FlashScooters/Challenge/vehicles"
        static let VEHICLES_DETAIL_END_POINT = "/FlashScooters/Challenge/vehicles/"
    }
}
