//
//  AppDelegate.swift
//  Flash
//
//  Created by Karan Kumar on 27/05/19.
//  Copyright © 2019 Karan Kumar. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    lazy var initializers: [Initializable] = [
        ThemeInitializer(),
        SVProgressHudInitializer()
    ]
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        initializers.forEach { $0.initialize() }
        
        let navigationBase = BaseNavigationController()
        navigationBase.setViewControllers([HomeRouter.createModule()], animated: true)
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = navigationBase
        self.window?.makeKeyAndVisible()
        
        return true
    }

}

