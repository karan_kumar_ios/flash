//
//  SVProgressHudInitializer.swift
//  Flash
//
//  Created by Karan Kumar on 27/05/19.
//  Copyright © 2019 Karan Kumar. All rights reserved.
//

import UIKit
import SVProgressHUD

class SVProgressHudInitializer: Initializable {
    
    func initialize() {
        SVProgressHUD.setForegroundColor(UIColor.flashThemeColor)
        SVProgressHUD.setDefaultStyle(.light)
    }
}
