//
//  ThemeInitializer.swift
//  Flash
//
//  Created by Karan Kumar on 27/05/19.
//  Copyright © 2019 Karan Kumar. All rights reserved.
//

import UIKit

class ThemeInitializer: Initializable {
    
    func initialize() {
        UINavigationBar.appearance(whenContainedInInstancesOf: [BaseNavigationController.self]).tintColor = .darkGray
        UINavigationBar.appearance(whenContainedInInstancesOf: [BaseNavigationController.self]).barTintColor = .white
        UINavigationBar.appearance(whenContainedInInstancesOf: [BaseNavigationController.self]).titleTextAttributes = [
            .foregroundColor: UIColor.black
        ]
    }
    
}
