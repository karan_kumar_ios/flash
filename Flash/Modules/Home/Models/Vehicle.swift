//
//  Vehicle.swift
//  Flash
//
//  Created by Karan Kumar on 28/05/19.
//  Copyright © 2019 Karan Kumar. All rights reserved.
//

import Foundation
import ObjectMapper


struct Vehicle: Mappable {
    var id: Int?
    var name: String?
    var description: String?
    var latitude: Double?
    var longitude: Double?
    var batteryLevel: Int?
    var timestamp: String?
    var price: Double?
    var priceTime: Int?
    var currency: String?
    
    init?(map: Map) {
        
    }
    
    mutating func mapping(map: Map) {
        id     <- map["id"]
        name   <- map["name"]
        description   <- map["description"]
        latitude   <- map["latitude"]
        longitude   <- map["longitude"]
        batteryLevel   <- map["batteryLevel"]
        timestamp   <- map["timestamp"]
        price   <- map["price"]
        priceTime   <- map["priceTime"]
        currency   <- map["currency"]
    }
}
