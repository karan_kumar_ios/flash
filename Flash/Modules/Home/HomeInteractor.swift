//
//  HomeInteractor.swift
//  Flash
//
//  Created by Karan Kumar on 28/05/19.
//  Copyright © 2019 Karan Kumar. All rights reserved.
//

import Foundation
import Alamofire

final class HomeInteractor {
    fileprivate let vehiclesService = VehiclesService()
}

// MARK: - Extensions

extension HomeInteractor: HomeInteractorInterface {
    
    @discardableResult
    func getVehicles(_ completion: @escaping VehiclesListCompletionBlock) -> DataRequest {
        return vehiclesService.getVehicles(completion)
    }
    
    @discardableResult
    func getVehicleDetail(vehicleId: Int, _ completion: @escaping VehicleDetailCompletionBlock) -> DataRequest {
        return vehiclesService.getVehicleDetail(vehicleId, completion)
    }
}
