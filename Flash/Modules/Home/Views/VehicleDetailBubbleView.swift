//
//  VehicleDetailBubbleView.swift
//  Flash
//
//  Created by Karan Kumar on 29/05/19.
//  Copyright © 2019 Karan Kumar. All rights reserved.
//

import UIKit

class VehicleDetailBubbleView: UIView {

    @IBOutlet weak var titleLabel: UILabel?
    @IBOutlet weak var batteryLabel: UILabel?
    @IBOutlet weak var priceLabel: UILabel?
    @IBOutlet weak var progressBar: UIProgressView?
    
    static func createInstance() -> VehicleDetailBubbleView {
        let newInstance = Bundle.main.loadNibNamed("VehicleDetailBubbleView", owner: self, options: nil)?[0] as? VehicleDetailBubbleView
        guard let finalInstance = newInstance else {
            return VehicleDetailBubbleView()
        }
        return finalInstance
    }
    
    func setVehicleData(_ vehicle: Vehicle?) {
        guard let vehicleData = vehicle else {
            return
        }
        
        titleLabel?.text = vehicleData.description ?? ""
        if let battery = vehicleData.batteryLevel {
            batteryLabel?.text = "\(battery)%"
            progressBar?.progress = Float(battery)/Float(100)
        }
        if let price = vehicleData.price, let currency = vehicleData.currency {
            priceLabel?.text = "\(price) \(currency)"
        }
    }

}
