//
//  HomePresenter.swift
//  Flash
//
//  Created by Karan Kumar on 28/05/19.
//  Copyright © 2019 Karan Kumar. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation
import MapKit
import Alamofire

class HomePresenter {
    
    // MARK: - Private properties
    private unowned var view: HomeViewInterface & VehicleDetailResponseInterface
    private var interactor: HomeInteractorInterface
    private var router: HomeRouterInterface
    
    private var vehicles: [Vehicle] = [] {
        didSet {
            view.reloadVehicles()
        }
    }
    
    private var vehiclesDetailMap: Dictionary<Int, Vehicle> = Dictionary()
    
    init(router: HomeRouterInterface, view: HomeViewInterface&VehicleDetailResponseInterface, interactor: HomeInteractorInterface) {
        self.router = router
        self.view = view
        self.interactor = interactor
    }
    
    private func getAnnotation(vehicle: Vehicle) -> FlashPointAnnotation {
        let vehicleAnnotation = FlashPointAnnotation(vehicle: vehicle)
        guard let lat = vehicle.latitude, let lon = vehicle.longitude else {
            return vehicleAnnotation
        }
        vehicleAnnotation.coordinate = CLLocationCoordinate2D(latitude: lat, longitude: lon)
        return vehicleAnnotation
    }
    
    private func setResponse(_ response: DataResponse<[Vehicle]>) {
        if let vehicles = response.result.value {
            self.vehicles = vehicles
        } else if response.result.error != nil {
            view.showErrorAlert(with: "Unable to fetch vehicles at the moment, Please try again")
        } else {
            view.showErrorAlert(with: "Unable to fetch vehicles at the moment, Please try again")
        }
    }
    
    private func parseResponse(_ vehicleId: Int, _ response: DataResponse<Vehicle>) {
        if let vehicle = response.result.value {
            vehiclesDetailMap[vehicleId] = vehicle
            view.didGetVehicleDetail(vehicle: vehicle) // notify view
        } else if response.result.error != nil {
            view.showErrorAlert(with: "Unable to fetch vehicle detail")
        } else {
            view.showErrorAlert(with: "Unable to fetch vehicles detail")
        }
    }
}

// MARK: - Extensions
extension HomePresenter: HomePresenterInterface {
    
    func fetchVehicles() {
        view.setLoadingVisible(true)
       
        interactor.getVehicles {[weak self] (response: DataResponse<[Vehicle]>) in
            self?.setResponse(response)
            
            self?.view.setLoadingVisible(false)
        }
    }
    
    func getSavedVehicleDetail(vehicleId: Int) -> Vehicle? {
        guard let vehicleDetail = vehiclesDetailMap[vehicleId] else {
            return nil
        }
        return vehicleDetail
    }
    
    func fetchVehicleDetail(vehicleId: Int) {
        interactor.getVehicleDetail(vehicleId: vehicleId) {[weak self] (response: DataResponse<Vehicle>) -> (Void) in
            self?.parseResponse(vehicleId, response)
        }
    }
    
    func getAllVehicles() -> [Vehicle] {
        if vehicles.count == 0 {
            fetchVehicles()
        }
        return vehicles
    }
    
    func getVehicleAnnotations() -> [FlashPointAnnotation] {
        let vehicles = getAllVehicles()
        
        var vehicleAnnotations = Array<FlashPointAnnotation>()
        vehicles.forEach {
            vehicleAnnotations.append(getAnnotation(vehicle: $0))
        }
        return vehicleAnnotations
    }
}



