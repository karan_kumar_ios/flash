//
//  FlashPointAnnotation.swift
//  Flash
//
//  Created by Karan Kumar on 28/05/19.
//  Copyright © 2019 Karan Kumar. All rights reserved.
//

import UIKit
import MapKit

class FlashPointAnnotation: MKPointAnnotation {
    
    var vehicle: Vehicle?

    init(vehicle: Vehicle) {
        self.vehicle = vehicle
    }
}
