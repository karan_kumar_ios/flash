//
//  HomeRouter.swift
//  Flash
//
//  Created by Karan Kumar on 27/05/19.
//  Copyright © 2019 Karan Kumar. All rights reserved.
//

import UIKit

class HomeRouter: HomeRouterInterface {
    
    static func createModule() -> UIViewController {
        let controller = mainstoryboard.instantiateViewController(ofType: HomeViewController.self)
        
        let interactor: HomeInteractorInterface = HomeInteractor()
        let router: HomeRouterInterface = HomeRouter()
        
        let presenter : HomePresenterInterface = HomePresenter(router: router, view: controller, interactor: interactor)
        
        controller.presentor = presenter
        return controller
    }
    
    private static var mainstoryboard: UIStoryboard{
        return UIStoryboard(name:"Main",bundle: Bundle.main)
    }
    /*
     All Navigation logic here for home
     */
    
    
}
