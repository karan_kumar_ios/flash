//
//  HomeInterfaces.swift
//  Flash
//
//  Created by Karan Kumar on 28/05/19.
//  Copyright © 2019 Karan Kumar. All rights reserved.
//

import Foundation
import Alamofire

protocol HomeRouterInterface: RouterInterface {
    // navigation func
}

protocol HomeViewInterface: ViewInterface {
    func reloadVehicles()
    func setLoadingVisible(_ visible: Bool)
}

protocol VehicleDetailResponseInterface: class {
    func didGetVehicleDetail(vehicle: Vehicle)
    func failToGetVehicleDetail(vehicleId: Int)
}

protocol HomePresenterInterface: class {
    func fetchVehicles()
    func getSavedVehicleDetail(vehicleId: Int) -> Vehicle?
    func fetchVehicleDetail(vehicleId: Int)
    func getAllVehicles() -> [Vehicle]
    func getVehicleAnnotations() -> [FlashPointAnnotation]
}

protocol HomeInteractorInterface: class {
    @discardableResult
    func getVehicles(_ completion: @escaping VehiclesListCompletionBlock) -> DataRequest
    
    @discardableResult
    func getVehicleDetail(vehicleId: Int, _ completion: @escaping VehicleDetailCompletionBlock) -> DataRequest
}
