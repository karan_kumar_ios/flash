//
//  HomeViewController.swift
//  Flash
//
//  Created by Karan Kumar on 28/05/19.
//  Copyright © 2019 Karan Kumar. All rights reserved.
//

import UIKit
import MapKit

class HomeViewController: UIViewController {

    var presentor:HomePresenterInterface!
    
    let mapPinIdentifier = "mapPinIdentifier"

    @IBOutlet private weak var map: MKMapView?
    @IBOutlet private weak var refreshBarItem: UIBarButtonItem?
    
    @IBAction func refreshVehicles() {
        map?.removeAnnotations(map?.annotations ?? [])
        presentor?.fetchVehicles()
    }
    
    private var currentlyOpenAnnotation: FlashPointAnnotation?
    
    // MARK: - Lifecycle -
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupSubview()
    }
    
    func setupSubview() {
        map?.delegate = self
        presentor?.fetchVehicles()
        self.navigationItem.title = "Search Vehicles"
    }
    
    func loadMapWithPins(annotations: [MKPointAnnotation]) {
        map?.removeAnnotations(map?.annotations ?? [])
        map?.addAnnotations(annotations)
        map?.showAnnotations(annotations, animated: true)
    }
    
    private func addEqualConstraint(_ view: UIView, attr: NSLayoutConstraint.Attribute, constant: CGFloat) {
        let constraint = NSLayoutConstraint(item: view, attribute: attr, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: constant)
        view.addConstraint(constraint)
    }
}

// MARK: - Extensions

extension HomeViewController: HomeViewInterface {
    func reloadVehicles() {
        let vehiclePins = presentor?.getVehicleAnnotations()
        guard let pins = vehiclePins else { return }
        
        loadMapWithPins(annotations: pins)
    }
    func setLoadingVisible(_ visible: Bool) {
        if visible {
            showProgressHUD()
        } else {
            hideProgressHUD()
        }
    }
    func showAlert(with title: String?, message: String?, actions: [UIAlertAction]) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        actions.forEach { alert.addAction($0) }
        self.present(alert, animated: true, completion: nil)
    }
}

extension HomeViewController: VehicleDetailResponseInterface {
    func didGetVehicleDetail(vehicle: Vehicle) {
        guard let annotation = currentlyOpenAnnotation else {
            return
        }
        /* check if currently open annotation is the one we requested for
         */
        if annotation.vehicle?.id == vehicle.id {
            map?.removeAnnotation(annotation)
            map?.addAnnotation(annotation)
            map?.selectAnnotation(annotation, animated: true)
        }
    }
    
    func failToGetVehicleDetail(vehicleId: Int) {
        if currentlyOpenAnnotation != nil && currentlyOpenAnnotation?.vehicle?.id == vehicleId {
            map?.deselectAnnotation(currentlyOpenAnnotation, animated: true)
            showErrorAlert(with: "Unable to get vehicle details")
        }
    }
}

extension HomeViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        guard annotation is MKPointAnnotation else { return nil }
        
        let annotationView = dequequeOrCreatePinView(mapView, annotation: annotation)
        
        guard let flashAnnotation = annotation as? FlashPointAnnotation else {
            return annotationView
        }
        let view = getCustomBubbleView(annotaiton: flashAnnotation)
        annotationView?.detailCalloutAccessoryView = view
        annotationView?.canShowCallout = true
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        guard let annotation = view.annotation, let flashAnnotation = annotation as? FlashPointAnnotation else {
            return
        }
        currentlyOpenAnnotation = flashAnnotation
        
        let associatedVehicle = flashAnnotation.vehicle
        guard let vehicleId = associatedVehicle?.id else {
            return
        }
        if presentor.getSavedVehicleDetail(vehicleId: vehicleId) == nil {
            // no vehicle detail found
            presentor.fetchVehicleDetail(vehicleId: vehicleId)
        }
    }
    func mapView(_ mapView: MKMapView, didDeselect view: MKAnnotationView) {
        currentlyOpenAnnotation = nil // reset
    }
  
}

extension HomeViewController {
    private func dequequeOrCreatePinView(_ mapView: MKMapView, annotation: MKAnnotation) -> MKAnnotationView? {
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: mapPinIdentifier)
        
        if annotationView == nil {
            annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: mapPinIdentifier)
            annotationView!.canShowCallout = true
        }
        annotationView?.annotation = annotation
        return annotationView
    }
    
    private func getCustomBubbleView(annotaiton: FlashPointAnnotation) -> UIView {
        let associatedVehicle = annotaiton.vehicle
        
        guard let vehicleId = associatedVehicle?.id, let vehicleDetail = presentor.getSavedVehicleDetail(vehicleId: vehicleId) else {
            // loader
            let loader = getDummyLoader()
            return loader
        }
        let view = getNewDetailBubble()
        view.setVehicleData(vehicleDetail)
        return view
    }
    
    private func getDummyLoader() -> UIActivityIndicatorView {
        let loader = UIActivityIndicatorView()
        loader.style = .gray
        loader.startAnimating()
        addEqualConstraint(loader, attr: .width, constant: 20)
        addEqualConstraint(loader, attr: .height, constant: 20)
        return loader
    }
    
    private func getNewDetailBubble() -> VehicleDetailBubbleView {
        let view = VehicleDetailBubbleView.createInstance()
        addEqualConstraint(view, attr: .width, constant: view.bounds.size.width)
        addEqualConstraint(view, attr: .height, constant: view.bounds.size.height)
        return view
    }
}
